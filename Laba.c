#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>              
#include <math.h>
#include <stdbool.h>
#include <float.h>
#include <malloc.h>
#include <stdlib.h>

void processor();
float calculator(int function, float a, float x);
float getUserInput(char* title);
float calcG(float a, float x);
float calcF(float a, float x);
float calcY(float a, float x);
void showResults(float results[], int lastIndex);
void saveToFile(FILE* fp, float results[], int lastIndex);
void clearStdin();

/// <summary>
/// The main method of the program
/// </summary>
/// <returns>Result</returns>
int main()
{
    while (true)
    {
        processor();
        printf("Input 'y' for continue or 'n' for exit: \n");
        char con;
        scanf_s("%c", &con, 1);
        clearStdin();
        if (con == "n")
        {
            break;
        }
    }
    return 0;
}

/// <summary>
/// The main method of the program
/// </summary>
void processor()
{
    float a = getUserInput("Enter a: \n");
    float x1 = getUserInput("Enter x1: \n");
    float x2 = getUserInput("Enter x2: \n");
    float dx = getUserInput("Enter dx: \n");
    float x = x1;
    int i = 0;

    float* results_g = NULL;
    float* results_f = NULL;
    float* results_y = NULL;

    while (x <= x2)
    {
        results_g = (float*)realloc(results_g, (i + 1) * sizeof(float));
        float result_g = calculator(1, a, x);
        results_g[i] = result_g;
        results_f = (float*)realloc(results_f, (i + 1) * sizeof(float));
        float result_f = calculator(2, a, x);
        results_f[i] = result_f;
        results_y = (float*)realloc(results_y, (i + 1) * sizeof(float));
        float result_y = calculator(3, a, x);
        results_y[i] = result_y;
        x += dx;
        if (x > x2) {
            break;
        }
        i++;
    }

    char* filename = "data.txt";
    FILE* fp;
    if ((fp = fopen(filename, "w")) == NULL)
    {
        perror("Error occured while opening file");
        return 1;
    }
    saveToFile(fp, results_g, i);
    saveToFile(fp, results_f, i);
    saveToFile(fp, results_y, i);
    fclose(fp);
    
    results_g[0] = '\0';
    results_f[0] = '\0';
    results_y[0] = '\0';
    
    if ((fp = fopen(filename, "r")) == NULL)
    {
        perror("Error occured while opening file");
        return 1;
    }

    char results[256];
    while ((fgets(results, 256, fp)) != NULL)
    {
        printf("Results:\n");
        printf("%s", results);
    }

    fclose(fp);
    free(results_g);
    free(results_f);
    free(results_y);
}

/// <summary>
/// Formula selection function
/// </summary>
/// <param name="function">Number of the selected function</param>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calculator(int function, float a, float x) 
{
    switch (function)
    {
    case 1:
        return calcG(a, x);  
    case 2:
        return calcF(a, x);
    case 3:
        return calcY(a, x);
    default: return 0;
    }
}
/// <summary>
/// Function for calculating the formula G
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcG(float a, float x)
{
    return (10 * (-45 * pow(a, 2) + 49 * a * x + 6 * pow(x, 2))) / (15 * pow(a, 2) + 49 * a * x + 24 * pow(x, 2));
}
/// <summary>
/// Function for calculating the formula F
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcF(float a, float x)
{
    return tan((5 * pow(a, 2) + 34 * a * x + 45 * pow(x, 2)) * M_PI / 180);
}
/// <summary>
/// Function for calculating the formula Y
/// </summary>
/// <param name="a">Function coefficient</param>
/// <param name="x">Function parameter</param>
/// <returns>Result</returns>
float calcY(float a, float x)
{
    return -1 * asin((7 * pow(a, 2) - a * x - 8 * pow(x, 2)) * M_PI / 180);
}
/// <summary>
/// Getting user Input
/// </summary>
/// <param name="title">Offer text</param>
/// <returns>Entered value</returns>
float getUserInput(char* title)
{
    printf(title);
    float p;
    if (!scanf_s("%f", &p))
    {
        printf("Input error!");
        return 0;
    }
    else
        return p;
}
/// <summary>
/// Show Results
/// </summary>
/// <param name="results"></param>
/// <param name="lastIndex"></param>
void showResults(float results[], int lastIndex)
{
    char res[1024] = "";
    for (int j = 0; j <= lastIndex; j++)
    {
        char buf[100];
        _gcvt(results[j], 6, buf);
        strcat(res, buf);
        strcat(res, " | ");
    }
    printf(res);
    printf("\n\n");
}
/// <summary>
/// Show Results
/// </summary>
/// <param name="results"></param>
/// <param name="lastIndex"></param>
void saveToFile(FILE* fp, float results[], int lastIndex)
{
    for (int j = 0; j <= lastIndex; j++)
    {
        char buf[100];
        _gcvt(results[j], 6, buf);
        fputs(buf, fp);
        fputs("   ", fp);
    }    
    fputs("\n", fp);
}
/// <summary>
/// Clear Stdin
/// </summary>
void clearStdin()
{
    while (getchar() != '\n');
}
